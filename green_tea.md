# Green Tea

**Description:**
Green tea is a type of tea made from Camellia sinensis leaves that haven't undergone the same withering and oxidation process used to make oolong teas and black teas. It is known for its fresh, grassy flavor and numerous health benefits, attributed to its high content of antioxidants and polyphenols.

**Features:**
- Type: Green tea
- Flavor: Fresh, grassy
- Benefits: Rich in antioxidants, potential health benefits
- Caffeine: Moderate

# Matcha Green Tea

**Description:**
Matcha is a finely ground powdered green tea, known for its vibrant green color and intense flavor. It's commonly used in traditional Japanese tea ceremonies and has a distinct umami taste.

**Features:**
- Type: Green tea
- Flavor: Umami, intense
- Benefits: High in antioxidants, energy-boosting
- Caffeine: Moderate

# Sencha Green Tea

**Description:**
Sencha is a traditional Japanese green tea with a delicate and slightly sweet flavor. It's characterized by its long, flat leaves and grassy aroma.

**Features:**
- Type: Green tea
- Flavor: Delicate, slightly sweet
- Benefits: Antioxidant-rich, mild caffeine
- Caffeine: Moderate

# Jasmine Green Tea

**Description:**
Jasmine green tea is scented with jasmine blossoms, creating a fragrant and floral infusion. The combination of jasmine and green tea offers a harmonious flavor profile.

**Features:**
- Type: Green tea
- Flavor: Floral, aromatic
- Benefits: Antioxidants, soothing
- Caffeine: Moderate

# Gunpowder Green Tea

**Description:**
Gunpowder green tea gets its name from its rolled leaf shape, resembling gunpowder pellets. It has a bold and slightly smoky flavor profile.

**Features:**
- Type: Green tea
- Flavor: Bold, slightly smoky
- Benefits: Antioxidants, energizing
- Caffeine: Moderate
