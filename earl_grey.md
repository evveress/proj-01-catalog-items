# Earl Grey Tea

**Description:**
Earl Grey is a classic black tea infused with the essence of bergamot orange. It has a distinct citrus aroma and flavor that sets it apart from regular black teas. The combination of rich black tea and the floral, fruity notes of bergamot make Earl Grey a favorite among tea enthusiasts.

**Features:**
- Type: Black tea
- Flavor: Bergamot orange
- Aroma: Citrusy
- Caffeine: Moderate

---

# Darjeeling Earl Grey

**Description:**
A variation of Earl Grey, Darjeeling Earl Grey combines the characteristic bergamot flavor with the delicate muscatel notes of Darjeeling black tea. It offers a unique blend of citrus and floral undertones.

**Features:**
- Type: Black tea
- Flavor: Bergamot, muscatel
- Aroma: Citrusy, floral
- Caffeine: Moderate

# Lady Grey

**Description:**
Similar to Earl Grey, Lady Grey features a black tea base with the addition of citrus flavors like orange and lemon. It provides a zesty and refreshing twist on the traditional Earl Grey.

**Features:**
- Type: Black tea
- Flavor: Citrus (orange, lemon)
- Aroma: Zesty
- Caffeine: Moderate

# Cream Earl Grey

**Description:**
Cream Earl Grey infuses the classic bergamot flavor with creamy notes, creating a smoother and richer taste experience.

**Features:**
- Type: Black tea
- Flavor: Bergamot, creamy
- Aroma: Citrusy, creamy
- Caffeine: Moderate

# Russian Earl Grey

**Description:**
Russian Earl Grey combines the citrusy notes of bergamot with a touch of spices, often including ingredients like cinnamon and cloves.

**Features:**
- Type: Black tea
- Flavor: Bergamot, spiced
- Aroma: Citrusy, spicy
- Caffeine: Moderate
